#!/bin/bash
while read database; do
    echo "Dumpling: $database"
    mysqldump --defaults-extra-file='~/dbserver-migration/env/old-server.cnf' $database > ~/dbserver-migration/storage/dumps/$database.sql
done < ~/dbserver-migration/storage/databases.txt

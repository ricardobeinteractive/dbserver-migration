#!/bin/bash
while read database; do
    echo "Creating: $database"
    mysql -u root -e "CREATE DATABASE \`$database\`"
done < ~/dbserver-migration/storage/databases.txt

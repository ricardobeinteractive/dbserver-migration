#!/bin/bash
while read database; do
    echo "Importing: $database"
    mysql -u root $database < ~/dbserver-migration/storage/dumps/$database.sql
done < ~/dbserver-migration/storage/databases.txt
